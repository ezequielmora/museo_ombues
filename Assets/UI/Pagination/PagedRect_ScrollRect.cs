using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace UI.Pagination
{
    [ExecuteInEditMode]
    public class PagedRect_ScrollRect : ScrollRect
    {
        public bool DisableDragging = false;
        public bool DisableScrollWheel = false;

        public bool isBeingDragged = false;

        // Fixes to enable parallel dragging
        [Header("Checkbox para habilitar la deteccion de dragging Horizontal y vertical para control del OnValueChanged Simultaneo")]
        [SerializeField]
        public bool enableHDrag = false;
        public bool enableVDrag = false;

        [Space(30)]

        [Header("Control flag variables - GET value on Runtime")]
        [SerializeField]
        public bool isHorizontalDragging = false;
        public bool isVerticalDragging = false;



        public bool ResetDragOffset = false;

        private bool notifyPagedRect = true;

        [SerializeField]
        private PagedRect _PagedRect = null;
        public PagedRect PagedRect
        {
            get
            {
                if (_PagedRect == null) _PagedRect = this.GetComponent<PagedRect>();
                return _PagedRect;
            }
        }

        private RectTransform m_contentRectTransform = null;
        protected RectTransform contentRectTransform
        {
            get
            {
                if (m_contentRectTransform == null) m_contentRectTransform = this.content.transform as RectTransform;
                return m_contentRectTransform;
            }
        }

        public PagedRect_Scrollbar ScrollBar;
        
        protected override void OnEnable()
        {
            base.OnEnable();

            // this makes the positioning calculations a little easier
            contentRectTransform.pivot = new Vector2(0, 1);
        }

        public override void OnScroll(PointerEventData data)
        {
            Debug.Log("is Scrolled n� times" + data);
            //return;
        }
        /*
        public override void OnPointerDown(PointerEventData data)
        {
            Debug.Log("OnPointerClick called.");
        }
        */
        public override void OnBeginDrag(PointerEventData eventData)
        {
            if (DisableDragging) return;

            ResetDragOffset = false;

            if (notifyPagedRect && PagedRect != null) PagedRect.OnBeginDrag(eventData);
                
            base.OnBeginDrag(eventData);

            isBeingDragged = true;

            Debug.Log("is started drag");

            if (enableHDrag)
            {
                isHorizontalDragging = true;
            }

            if (enableVDrag)
            {
                isVerticalDragging = true;
            }
        }

        public override void OnDrag(PointerEventData eventData)
        {
            if (DisableDragging) return;

            if (!isBeingDragged) return;

            // If this is a horizontal PagedRect, only accept horizontal drag events, and vice versa if this is a Vertical PagedRect
            var analysis = AnalyseDragEvent(eventData);
            if (this.horizontal && analysis.DragPlane != DragEventAnalysis.eDragPlane.Horizontal) return;
            if (this.vertical && analysis.DragPlane != DragEventAnalysis.eDragPlane.Vertical) return;
            
            if (ResetDragOffset)
            {
                notifyPagedRect = false;
                
                OnEndDrag(eventData);
                OnBeginDrag(eventData);

                notifyPagedRect = true;
            }

            if(PagedRect != null) PagedRect.OnDrag(eventData);

            base.OnDrag(eventData);

            //Fixes para detectar multiples scrollers en paralelo
            if (enableHDrag)
            {
                if (!isHorizontalDragging)
                {
                    return;
                }
            }

            if (enableVDrag)
            {
                if (!isVerticalDragging)
                {
                    return;
                }
            }
        }

        public override void OnEndDrag(PointerEventData eventData)
        {
            if (DisableDragging) return;

            if (!isBeingDragged) return;

            // we're no longer being dragged
            isBeingDragged = false;
            
            // Notify PagedRect (so it can handle any OnEndDrag events if necessary)
            if(notifyPagedRect && PagedRect != null) PagedRect.OnEndDrag(eventData);

            base.OnEndDrag(eventData);

            //Fixes para detectar multiples scrollers en paralelo
            if (enableHDrag)
            {
                if (!isHorizontalDragging)
                {
                    return;
                }
                isHorizontalDragging = false;
            }

            if (enableVDrag)
            {
                if (!isVerticalDragging)
                {
                    return;
                }
                isVerticalDragging = false;
            }
        }
        
        public DragEventAnalysis AnalyseDragEvent(PointerEventData data)
        {
            return new DragEventAnalysis(data);            
        }


        private static Vector3 horizontalVector = new Vector2(1, 0);
        private static Vector3 verticalVector = new Vector2(0, -1);

        public Vector2 GetDirectionVector()
        {
            return horizontal ? horizontalVector : verticalVector;
        }

        public float GetOffset()
        {
            return (horizontal ? -content.anchoredPosition.x : content.anchoredPosition.y);
        }

        public float GetTotalSize()
        {
            return horizontal ? content.rect.width : content.rect.height;
        }

        public float GetPageSize()
        {
            return horizontal ? PagedRect.sizingTransform.rect.width : PagedRect.sizingTransform.rect.height;
        }
    }
}
