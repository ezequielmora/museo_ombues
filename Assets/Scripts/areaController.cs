﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// controlador para editar el area de posicionamiento de los objetos simulando realidad aumentada
public class areaController : MonoBehaviour {

    [Header("Grilla de edicion de la mesa en edit mode")]
    //public SpriteRenderer mesaPrincipal;
    //public MeshRenderer bordeMesaIzq;
    //public MeshRenderer bordeMesaDer;
    //public RawImage fondoCalibracion;
    public List<GameObject> objetosDeEdicion;

    [Header("Velocidad de movimiento de los elementos de edicion")]
    public float speed = 5f;

    public bool isEditable = false;


    [Header("Gameobjects que mostraran la informacion")]
    public Text posX;
    public Text posY;
    public Text posZ;

    public Text rotX;
    public Text rotY;
    public Text rotZ;
    public Text rotW;
    

    public RectTransform editMenu;

    public GameObject editArea;

    private Vector3 initialPosition;

    private Transform editAreaTransform;




    public static areaController instance;


    //Condicion para singleton
    private void Awake()
    {
        if(instance == null)
        {


            instance = this;


        } else if (instance != this){


            Destroy(this);


        }

        DontDestroyOnLoad(this);
    }



    // Use this for initialization
    void Start () {

        editAreaTransform = editArea.GetComponent<Transform>();

        initialPosition = editAreaTransform.transform.position;

        isEditable = false;


        /// Variables de inicio
        /// 
        //mesaPrincipal.gameObject.SetActive(false);
        //bordeMesaIzq.gameObject.SetActive(false);
        //bordeMesaDer.gameObject.SetActive(false);
        //fondoCalibracion.gameObject.SetActive(false);

        foreach (var item in objetosDeEdicion)
        {
            item.SetActive(false);
        }

        // ST LOAD DATA MANEJAR CON L
        //LoadData();

    }
	
	// Update is called once per frame
	void Update () {

        // NEW CALIBRATION
        if (Input.GetKeyDown(KeyCode.Space))
        {

            if (!isEditable)
            {
                //mesaPrincipal.gameObject.SetActive(true);
                //bordeMesaIzq.gameObject.SetActive(true);
                //bordeMesaDer.gameObject.SetActive(true);
                //fondoCalibracion.gameObject.SetActive(true);

                foreach (var item in objetosDeEdicion)
                {
                    item.SetActive(true);
                }

                isEditable = true;
            } else
            {
                //mesaPrincipal.gameObject.SetActive(false);
                //bordeMesaIzq.gameObject.SetActive(false);
                //bordeMesaDer.gameObject.SetActive(false);
                //fondoCalibracion.gameObject.SetActive(false);

                foreach (var item in objetosDeEdicion)
                {
                    item.SetActive(false);
                }

                isEditable = false;


            }
        }

        // OLD CALIBRATION
        /*
        posX.text = editAreaTransform.position.x.ToString();
        posY.text = editAreaTransform.position.y.ToString();
        posZ.text = editAreaTransform.position.z.ToString();

        rotX.text = editAreaTransform.rotation.x.ToString();
        rotY.text = editAreaTransform.rotation.y.ToString();
        rotZ.text = editAreaTransform.rotation.z.ToString();
        rotW.text = editAreaTransform.rotation.w.ToString();

        if (Input.GetKeyDown(KeyCode.Space))
        {

            isEditable = !isEditable;
            editMenu.gameObject.SetActive(true);

            mesaPrincipal.gameObject.GetComponent<SpriteRenderer>().enabled = true;
            bordeMesaIzq.gameObject.GetComponent<MeshRenderer>().enabled = true;
            bordeMesaDer.gameObject.GetComponent<MeshRenderer>().enabled = true;

           

        }

        if (!isEditable)
        {

            editMenu.gameObject.SetActive(false);


            mesaPrincipal.gameObject.GetComponent<SpriteRenderer>().enabled = false;
            bordeMesaIzq.gameObject.GetComponent<MeshRenderer>().enabled = false;
            bordeMesaDer.gameObject.GetComponent<MeshRenderer>().enabled = false;

            webcamController.instance.StartCamera();


        }


        if (isEditable)
        {

            MoveArea();

            webcamController.instance.StopCamera();


            if (Input.GetKeyDown(KeyCode.Q))
            {
                // Player prefs logic to save data into data folderf
                SaveData();
            }


            if (Input.GetKeyDown(KeyCode.R))
            {
                // Resetting position
                resetButton();
            }


            if (Input.GetKeyDown(KeyCode.L))
            {
                LoadData();
            }

        }

        */

        

    }


    //TODO: Modo de edicion para Definir ubicacion de la area editable de la mesa
    void MoveArea()
    {

        float translation = Input.GetAxis("Vertical") * speed * Time.deltaTime;

        float rotation = Input.GetAxis("Horizontal") * speed * Time.deltaTime;

        float planeFix = Input.GetAxis("Mouse Y") * speed * Time.deltaTime;

        editAreaTransform.Translate(0, planeFix, translation);

        editAreaTransform.Rotate(0, rotation, 0);

    }

    // resetea la posicion inicial del area de la mesa
    public void resetButton()
    {


        // ST
        //editArea.transform.position = initialPosition;

        print("resetting position");

        editAreaTransform.position = new Vector3(0f, 0f, 0f);
        editAreaTransform.rotation = new Quaternion(0f, 25f, 0f, editAreaTransform.rotation.w);

    }


    public void SaveData()
    {
        print("saving data");

        PlayerPrefs.SetFloat("x_position", editAreaTransform.position.x);
        PlayerPrefs.SetFloat("y_position", editAreaTransform.position.y);
        PlayerPrefs.SetFloat("z_position", editAreaTransform.position.z);

        PlayerPrefs.SetFloat("x_rotation", editAreaTransform.rotation.x);
        PlayerPrefs.SetFloat("y_rotation", editAreaTransform.rotation.y);
        PlayerPrefs.SetFloat("z_rotation", editAreaTransform.rotation.z);
        PlayerPrefs.SetFloat("w_rotation", editAreaTransform.rotation.w);



        PlayerPrefs.Save();

    }


    public void LoadData()
    {
        float tempXPos = 0;
        float tempYPos = 0;
        float tempZPos = 0;

        float tempXRot = 0;
        float tempYRot = 0;
        float tempZRot = 0;
        float tempWRot = 0;


        if (PlayerPrefs.HasKey("x_position"))
            tempXPos = PlayerPrefs.GetFloat("x_position", -5.480591f);
        if (PlayerPrefs.HasKey("y_position"))
            tempYPos = PlayerPrefs.GetFloat("y_position", -0.9645159f);
        if (PlayerPrefs.HasKey("z_position"))
            tempZPos = PlayerPrefs.GetFloat("Z_position", 5.034664f);


        if (PlayerPrefs.HasKey("x_rotation"))
            tempXRot = PlayerPrefs.GetFloat("x_rotation", -3.808f);
        if (PlayerPrefs.HasKey("y_rotation"))
            tempYRot = PlayerPrefs.GetFloat("y_rotation", 25.817f);
        if (PlayerPrefs.HasKey("z_rotation"))
            tempZRot = PlayerPrefs.GetFloat("z_rotation", -0.026f);
        if (PlayerPrefs.HasKey("w_rotation"))
            tempWRot = PlayerPrefs.GetFloat("w_rotation", 0);

        //objectPlacementController.Instance.TabletAreaPlane.transform.position.Set(tempXPos, tempYPos, tempZPos);
        //objectPlacementController.Instance.TabletAreaPlane.transform.rotation.Set(tempXRot, tempYRot, tempZRot, tempWRot);

        editAreaTransform.position = new Vector3(tempXPos, tempYPos, tempZPos);
        editAreaTransform.rotation = new Quaternion(tempXRot, tempYRot, tempZRot, tempWRot);

        print("Loading...." + tempXPos);
    }



}
