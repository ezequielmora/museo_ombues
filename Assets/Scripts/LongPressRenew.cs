﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TouchScript.Gestures;

public class LongPressRenew : MonoBehaviour {

    private LongPressGesture myLongPress;

    private void OnEnable()
    {

        myLongPress = GetComponent<LongPressGesture>();
        myLongPress.StateChanged += myLongPressHandler;

    }

    private void OnDisable()
    {

        myLongPress.StateChanged -= myLongPressHandler;

    }


    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}


    private void myLongPressHandler(object sender, GestureStateChangeEventArgs e)
    {
        if(e.State == Gesture.GestureState.Recognized && objectPlacementController.Instance.objectsTracking != null)
        {

            objectPlacementController.Instance.RenewTable();

        }
    }
}
