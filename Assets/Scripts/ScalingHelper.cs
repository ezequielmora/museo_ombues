﻿using System.Collections;
using System.Collections.Generic;
using TouchScript.Gestures;
using UnityEngine;

public class ScalingHelper : MonoBehaviour {

    private TransformGesture myTransformGesture;

	// Use this for initialization
	void OnEnable () {

        myTransformGesture = GetComponent<TransformGesture>();
        myTransformGesture.Transformed += scaleHander;
	}
	
	// Update is called once per frame
	void OnDisable () {


        myTransformGesture.Transformed -= scaleHander;


    }


    private void scaleHander(object sender, System.EventArgs e)
    {
        print("scale handling............");
        //transform.localScale = myTransformGesture.transform.localScale;

        Vector3 scaleLimit = new Vector3(0.2f,0.2f,0.2f);

        if (transform.localScale.x > scaleLimit.x && transform.localScale.y > scaleLimit.y && transform.localScale.z > scaleLimit.z)
        {
            transform.localScale = scaleLimit;
            print("scale over limit..............");
        }
    }

}
