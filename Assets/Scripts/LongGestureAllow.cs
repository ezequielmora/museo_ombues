﻿using System;
using System.Collections;
using System.Collections.Generic;
using TouchScript.Gestures;
using UnityEngine;

public class LongGestureAllow : MonoBehaviour {

    private PressGesture myLongPress;
    private ReleaseGesture myRelease;

    private previewHandler myPreviewHandler;

    private void OnEnable()
    {

        myLongPress = GetComponent<PressGesture>();
        myLongPress.Pressed += myLongPressHandler;

        myRelease = GetComponent<ReleaseGesture>();
        myRelease.Released += myReleaseHandler;


    }

    private void OnDisable()
    {

        myLongPress.Pressed -= myLongPressHandler;
        myRelease.Released -= myReleaseHandler;


    }


    private void myLongPressHandler(object sender, EventArgs e)
    {
    

            myPreviewHandler.allowDragging = true;
            Debug.Log("Allow recognized................");
        

    }

    private void myReleaseHandler(object sender, EventArgs e)
    {

        myPreviewHandler.allowDragging = false;
        Debug.Log("Prohibited recognized...................");

    }


    // Use this for initialization
    void Start () {

        myPreviewHandler = GetComponent<previewHandler>();


    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
