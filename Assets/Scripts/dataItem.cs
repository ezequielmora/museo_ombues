﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class dataItem {

    [SerializeField]
    private int id;
    public int Id { get { return id; } set { id = value; } }

    [SerializeField]
    private string name;
    public string Name { get { return name; } set { name = value; } }

    [SerializeField]
    private Sprite imageButton;
    public Sprite ImageButton { get { return imageButton; } set { imageButton = value; } }

    [SerializeField]
    private Sprite imageBig;
    public Sprite ImageBig { get { return imageBig; } set { imageBig = value; } }

    [SerializeField]
    private string title;

    public string Title
    {

        get

        {

            return title;

        }

        set

        {

            title = value;

        }

    }

    [SerializeField]
    private string dataText;

    public string DataText
    {

        get

        {

            return dataText;

        }

        set

        {

            dataText = value;

        }

    }

}
