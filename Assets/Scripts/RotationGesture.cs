﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TouchScript;
using TouchScript.Gestures;
using System;

public class RotationGesture : MonoBehaviour {


    [Header("INVIERTE EL SENTIDO DE LA ROTACION")]
    public bool isInverted = false;


    private Renderer myRenderer;

    private previewHandler myPreviewHandler;

    private void OnEnable()
    {
        GetComponent<PinnedTransformGesture>().TransformStarted += RotationHandler;
        GetComponent<PinnedTransformGesture>().TransformCompleted += FinishRotationHandler;

        GetComponent<TransformGesture>().TransformStarted += ScaleHandler;
        GetComponent<TransformGesture>().TransformCompleted += FinishScalingHandler;

    }

    private void OnDisable()
    {
        GetComponent<PinnedTransformGesture>().TransformStarted -= RotationHandler;
        GetComponent<PinnedTransformGesture>().TransformCompleted -= FinishRotationHandler;

        GetComponent<TransformGesture>().TransformStarted -= ScaleHandler;
        GetComponent<TransformGesture>().TransformCompleted -= FinishScalingHandler;

    }


    private void RotationHandler(object sender, EventArgs e)
    {
        // No hace falta comparacion de estado porque en el mismo OnEnable declara el estado
        //if(e.State = Gesture.GestureState.Recognized)

        /*
        myRenderer.material.color = Color.green;

        Debug.Log("Rotating................");
        */

        myPreviewHandler.allowDragging = false;
        Debug.Log("ROTATION PROHIBITED DRAGGING");

        // Cancela gesto de escalado al detectar rotacion
        //GetComponent<TransformGesture>().Cancel();
        //Debug.Log("Cancel SCALING.....");



        //GetComponent<PinnedTransformGesture>().CanPreventGesture(GetComponent<TransformGesture>());

        // Si esta el checkbox invierte el sentido de la rotacion

        //ST
        /*
        if (isInverted)
        {
            transform.localRotation = new Quaternion(transform.localRotation.x, -transform.localRotation.y, transform.localRotation.z, transform.localRotation.w) * Quaternion.AngleAxis(GetComponent<PinnedTransformGesture>().DeltaRotation, GetComponent<PinnedTransformGesture>().RotationAxis);
            //transform.localRotation  *= Quaternion.AngleAxis(GetComponent<PinnedTransformGesture>().DeltaRotation, GetComponent<PinnedTransformGesture>().RotationAxis);
        }
        */
        //
    }


    private void FinishRotationHandler(object sender, EventArgs e)
    {
        // No hace falta comparacion de estado porque en el mismo OnEnable declara el estado
        //if(e.State = Gesture.GestureState.Recognized)

        /*
        myRenderer.material.color = Color.white;

        Debug.Log("Rotating stop................");
        */

        myPreviewHandler.allowDragging = true;
        Debug.Log("ROTATION ALLOWING DRAGGING");
    }



    private void ScaleHandler(object sender, EventArgs e)
    {

        myPreviewHandler.allowDragging = false;
        Debug.Log("SCALE PROHIBITED DRAGGING");

        // Cancela el gesto de rotacion al detectar el escalado
        //GetComponent<PinnedTransformGesture>().Cancel(true, true);
        //Debug.Log("Cancel ROTATION.....");

        //GetComponent<TransformGesture>().CanPreventGesture(GetComponent<PinnedTransformGesture>());

    }



    private void FinishScalingHandler(object sender, EventArgs e)
    {
        myPreviewHandler.allowDragging = true;
        Debug.Log("SCALE ALLOWING DRAGGING");
    }


    // Use this for initialization
    void Start () {
        /*
        myRenderer = GetComponent<Renderer>();
        */
        myPreviewHandler = GetComponent<previewHandler>();

        GetComponent<PinnedTransformGesture>().CanPreventGesture(GetComponent<TransformGesture>());
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
