﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TouchScript;
using UnityEngine.EventSystems;
using TouchScript.Gestures;

public class previewHandler : MonoBehaviour, IDragHandler // IPointerUpHandler, // IPointerDownHandler
{

    [Header("Factor de Escala permitido para el gesto de escalado dinamico")]
    public float scaleFactor = 2f;

    //"Limite Maximo y minimo para el escalado de los objetos"
    [HideInInspector]
    public float minimumScale;
    [HideInInspector]
    public float maximumScale;

    [Space(30)]

    private Renderer myRenderer;

    // Por defecto permite hacer el dragging de objetos sino intervienen los otros 2 gestos
    [HideInInspector]
    public bool allowDragging = true;


    public float rotationSpeed = 5f;

    [HideInInspector]
    public bool insideArea = true;

    public Vector3 screenPoint;

    public Vector3 offset;

    // Bloque de informacion adicional
    public RectTransform bloqueInfo;


    private Vector3 touchMovePosition;
    private bool isMoving = false;

    private void OnEnable()
    {
        if (TouchManager.Instance != null)
        {
            //TouchManager.Instance.TouchesBegan += touchesBeganHandler;
            TouchManager.Instance.TouchesMoved += touchesMovedHandler;
            //TouchManager.Instance.TouchesEnded += touchesEndedHandler;
        }
    }

    private void OnDisable()
    {
        if (TouchManager.Instance != null)
        {
            //TouchManager.Instance.NumberOfTouches -=
            //TouchManager.Instance.TouchesBegan -= touchesBeganHandler;
            TouchManager.Instance.TouchesMoved -= touchesMovedHandler;
            //TouchManager.Instance.TouchesEnded -= touchesEndedHandler;

        }
    }

    private void touchesBeganHandler(object sender, TouchEventArgs e)
    {

    }


    private void touchesMovedHandler(object sender, TouchEventArgs e)
    {

        //Debug.Log(e.Touches[0]);
        //Debug.Log(e.Touches[0].Tags + " touched hold at " + e.Touches[0].Position);

        // Sacar que funcione con el gesto del index 0 - mejor que detecte cuando hay un unico gesto para evitar movimiento en los demas gestos
        //ST// touchMovePosition = e.Touches[0].Position;

        if(TouchManager.Instance.NumberOfTouches == 1)
        {
            for (int i = 0; i < e.Touches.Count; i++)
            {
                touchMovePosition = e.Touches[0].Position;
                Debug.Log("Deteccion toque single y movimiento....................");
            }
        }

        //isMoving = true;

    }

    private void touchesEndedHandler(object sender, TouchEventArgs e)
    {

        //Debug.Log(e.Touches[0]);
        //Debug.Log(e.Touches[0].Tags + " touched hold at " + e.Touches[0].Position);

        //isMoving = false;

    }



    // Use this for initialization
    void Start () {

        minimumScale = transform.localScale.x;
        maximumScale = minimumScale * scaleFactor;

        myRenderer = GetComponent<Renderer>();

        allowDragging = true;
		
	}
	
	// Update is called once per frame
	void Update () {

        // Quitar rotacion
        //Preview();
		
	}




    //TODO: Comportamiento del objeto mientras esta en modo preview (mientras no este en el area)
    void Preview()
    {

        transform.Rotate(Vector3.up * Time.deltaTime * rotationSpeed);


    }
    // Cuando hace click con el boton del mouse y detecta presion siempre y cuando exista un collider en el objeto guarda la offset

    //Manejo de cartel viejo
    /*
    public void OnPointerDown(PointerEventData data)
    //ST void OnMouseDown()
    {

        //screenPoint = Camera.main.WorldToScreenPoint(transform.position);
        //offset = transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z));

        objectPlacementController.Instance.DisplayInfo();

        Debug.Log("is Entering...");

    }
    */
    
    //mientras mantiene ese click iguala la posicion del objeto a la posicion del cursor (siempre y cuando exista un collider)
    //ST void OnMouseDrag()
    //void FixedUpdate()
    public void OnDrag(PointerEventData eventData)
    {
        //Debug.Log("is Dragging...");

        if (!objectPlacementController.Instance.useTouchScript)
        {
            //if (insideArea)
            //{
            //Vector3 cursorScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);
            //Vector3 cursorPosition = Camera.main.ScreenToWorldPoint(cursorScreenPoint) + offset;
            //transform.position = cursorPosition;

            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, 100))
            {

                if (hit.rigidbody != null)
                //if(hit.collider.isTrigger && hit.collider.gameObject.tag == "Mesa" && isMoving)
                //if (hit.collider.gameObject.tag == "Created")
                {



                    //for (int i = 0; i < objectPlacementController.Instance.objectsToPlace.Count; i++)
                    //{

                    // la posicion del objeto que tenga este script es igual a la posicion del hit point
                    if (allowDragging)
                    {
                        transform.position = hit.point; //+ objectPlacementController.instance.positionOffset[i];
                                                        //Debug.Log("POSICION MOUSE: " + Input.mousePosition);
                    }

                    //}



                }


                else
                {

                    //insideArea = false;


                }

            }
            //}
            // sino esta dentro del area el objeto vuelve a modo previsualizacion: tamaño escalado 1.5 y rotando
            //else
            //{

            //}
        } else
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(touchMovePosition);
            if (Physics.Raycast(ray, out hit, 100))
            {

                if (hit.rigidbody != null)
                {



                    //for (int i = 0; i < objectPlacementController.Instance.objectsToPlace.Count; i++)
                    //{

                    // la posicion del objeto que tenga este script es igual a la posicion del hit point
                    if (allowDragging)
                    {
                        transform.position = hit.point; //+ objectPlacementController.instance.positionOffset[i];
                                                        //Debug.Log("POSICION TOUCH: " +  touchMovePosition);
                    }

                    //}



                }


                else
                {

                    //insideArea = false;


                }

            }
        }


    }

    // Metodo que recibe la llamada de SendMessage cada vez que se detecta ese tipo de gesto
    public void OnGestureStateChanged(Gesture sender)
    {
        if(sender.State == Gesture.GestureState.Recognized)
        {
            myRenderer.material.color = Color.green;

            Debug.Log("Rotating................");
        }

        
    }

    // Cuando suelto el cubo que arrastro lo dejo en la posicion del area deseada, sino esta dentro del area, lo elimina
    //ST// - void OnMouseUp()
    // Manejo de cartel viejo
    /*
    public void OnPointerUp(PointerEventData eventData)
    {

        objectPlacementController.Instance.HideInfo();

        if (!insideArea)
        {

            Destroy(this.gameObject);

        }

    }
    */
    /* ST //
    private void FixedUpdate()
    {
        if (objectPlacementController.Instance.useTouchScript && isMoving)
        {
            


            //if (insideArea)
            //{
            //Vector3 cursorScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);
            //Vector3 cursorPosition = Camera.main.ScreenToWorldPoint(cursorScreenPoint) + offset;
            //transform.position = cursorPosition;

            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(touchMovePosition);

            //if(GetComponent<Collider>().Raycast(ray, out hit, 100))

            if (Physics.Raycast(ray, out hit, 100))
            {

                if (hit.rigidbody != null)
                //if(hit.collider.isTrigger && hit.collider.gameObject.tag == "Mesa" && isMoving)
                //if (hit.collider.gameObject.tag == "Created")
                {

                    // Funcion que llama interfaz mientras hago drag
                    //ST objectPlacementController.Instance.DisplayInfo();
                    
                    for (int i = 0; i < objectPlacementController.Instance.objectsToPlace.Count; i++)
                    {

                        // la posicion del objeto que tenga este script es igual a la posicion del hit point
                        transform.position = hit.point; //+ objectPlacementController.instance.positionOffset[i];
                        Debug.Log("POSICION MOUSE: " + Input.mousePosition);

                    }

                    //this.gameObject.transform.position = hit.point; //+ objectPlacementController.instance.positionOffset[i];
                    //Debug.Log("POSICION MOUSE: " + Input.mousePosition);

                }


                else
                {

                    //insideArea = false;


                }

            }
            //}
            // sino esta dentro del area el objeto vuelve a modo previsualizacion: tamaño escalado 1.5 y rotando
            //else
            //{

            //}
        }
    }
    */

    /*
    public void DisplayInfo()
    {

        bloqueInfo.gameObject.SetActive(true);

    }

    public void HideInfo()
    {

        bloqueInfo.gameObject.SetActive(false);

    }
    */

}
