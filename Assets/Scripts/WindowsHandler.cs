﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WindowsHandler : MonoBehaviour {


    [Header("Numero de pagina y pagina por defecto")]
    public int indexNumber;
    public GameObject indexPage;

    [Header("Toggle Componenent del empapelado")]
    public Toggle toggleComponent;
    [Header("GameObject del Empapelado")]
    public GameObject empapeladoObject;

    [HideInInspector]
    public bool learpON = false;
    [HideInInspector]
    public bool learpOFF = false;

    [HideInInspector]
    public float learpStart = 0;
    private float learpDuration = 1;
    private float alpha = 0f;


    [Header("Lista de paginas")]
    public List<GameObject> Pages;

    private bool pressed = false;

    private GameObject tempIndex;


    private float timeCounter = 0f;
    private float timeLimit = 60f;

	// Use this for initialization
	void Start () {

        toggleComponent = toggleComponent.GetComponent<Toggle>();
        /*
        toggleComponent.onValueChanged.AddListener(delegate {
            ToggleChanged(toggleComponent);
        });
        */

	}

    public void ToggleChanged(Toggle on)
    {
        Debug.Log("Print Toggle changed");

        if (toggleComponent.isOn)
        {
            //ST empapeladoObject.SetActive(true);
            learpON = true;
            learpStart = Time.time;
            Debug.Log("sacar /  empapelado empapelado");
        } else
        {
            learpOFF = true;
            learpStart = Time.time;
            //ST empapeladoObject.SetActive(false);
            Debug.Log(" / agregar empapelado empapelado");
        }
  

    
        /*
        if (!empapeladoObject.activeInHierarchy)
        {
            empapeladoObject.SetActive(true);
            Debug.Log("activar empapelado");
        }
        */

    }

    // Update is called once per frame
    void Update () {

        if (pressed)
        {
            timeCounter += Time.deltaTime;

        }

        if(timeCounter > timeLimit)
        {
            ClosePage();
        }

        if (learpON)
        {
            //empapeladoObject.GetComponent<RawImage>().color = Color.Lerp(new Color(1,1,1,1), new Color(1, 1, 1, 0), Mathf.(Time.time, 1));

            toggleComponent.interactable = false;

            float learpProgress = Time.time - learpStart;
            empapeladoObject.GetComponent<RawImage>().color = Color.Lerp(new Color(1.0f,1.0f, 1.0f, 0.0f), new Color(1.0f, 1.0f, 1.0f, 1.0f), learpProgress / learpDuration);
            if(learpDuration < learpProgress)
            {
                learpON = false;
                toggleComponent.interactable = true;
            }
            

            /*
            float lerp = Mathf.PingPong(Time.time, learpDuration) / learpDuration;

            alpha = Mathf.Lerp(0.0f, 1.0f, lerp);

            empapeladoObject.GetComponent<RawImage>().color = new Color(1.0f,1.0f,1.0f, alpha);
            */
        }


        if (learpOFF)
        {
            //empapeladoObject.GetComponent<RawImage>().color = Color.Lerp(new Color(1,1,1,1), new Color(1, 1, 1, 0), Mathf.(Time.time, 1));

            toggleComponent.interactable = false;


            float learpProgress = Time.time - learpStart;
            empapeladoObject.GetComponent<RawImage>().color = Color.Lerp(new Color(1.0f, 1.0f, 1.0f, 1.0f), new Color(1.0f, 1.0f, 1.0f, 0.0f), learpProgress / learpDuration);
            if (learpDuration < learpProgress)
            {
                learpOFF = false;
                toggleComponent.interactable = true;

            }


            /*
            float lerp = Mathf.PingPong(Time.time, learpDuration) / learpDuration;

            alpha = Mathf.Lerp(0.0f, 1.0f, lerp);

            empapeladoObject.GetComponent<RawImage>().color = new Color(1.0f,1.0f,1.0f, alpha);
            */
        }


        /*
        if (learpON)
        {
            empapeladoObject.GetComponent<RawImage>().color = Color.Lerp(new Color(1, 1, 1, 0), new Color(1, 1, 1, 1), Mathf.PingPong(Time.time, 1));
        }
        */
    }


    public void NewPage(int newNumber)
    {
        /*
        if(indexNumber == 0)
        {
            if (!pressed)
            {

                indexNumber = newNumber;

                indexPage = Pages[newNumber];

                ChangePage();
                //pressed = true;
            }
        }

        if(newNumber == 0)
        {
            pressed = true;
        }
        */

        if(indexNumber == newNumber)
        {
            if (!pressed)
            {

                tempIndex = indexPage;

                ChangePage();

            }
            


        }


        if (indexNumber != newNumber)
        {

            pressed = false;


            tempIndex = indexPage;


            ClosePage();

            indexNumber = newNumber;

            indexPage = Pages[newNumber];


            ChangePage();


        }
    }


    public void ChangePage()
    {

        pressed = true;

        StartCoroutine(ChangingPage());

        //Invoke("ClosePage", 20);

    }

    public void ClosePage()
    {

        StartCoroutine(ClosingPage());

    }


    public IEnumerator ClosingPage()
    {
        pressed = false;
        timeCounter = 0;

        indexPage.GetComponent<Animator>().SetTrigger("exit");
        yield return new WaitForSeconds(0.250f);
        // Poner Animaciones
        indexPage.SetActive(false);
        tempIndex.SetActive(false);

        yield return new WaitForSeconds(0.1f);

        Debug.Log("pagina seteada en false");
        

        yield return null;
    }


    public IEnumerator ChangingPage()
    {
        // Poner Animaciones
        yield return new WaitForSeconds(0.5f);
        indexPage.SetActive(true);
        indexPage.GetComponent<Animator>().SetTrigger("entry");

        //pressed = true;

        Debug.Log("Changing page");

        yield return null;

        /*
        yield return new WaitForSeconds(20f);
        Debug.Log("Closing Page");
        ClosePage();
        */
    }

}
