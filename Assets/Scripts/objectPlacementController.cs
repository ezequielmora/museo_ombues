﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TouchScript;

public class objectPlacementController : MonoBehaviour {

    [Header("Tiempo por inactividad que se limpiara la mesa")]
    public float renewSeconds;
    private float renewCounter;
    private bool activateCounter = false;

    [Header("Tiempo por inactividad que se ejecutará el screensaver")]
    public GameObject ScreenSaver;
    public float renewScreenSaverSeconds;
    private float renewScreenSaverCounter;
    private bool activateScreenSaverCounter = false;


    [Header("Determina si el cursos y el visualizer son visibles")]
    public bool cursorIsVisible = true;
    public GameObject visualizerObject;

    // TODO: Distribucion y posicionamiento de objetos sobre el area de la mesa

    [Header("Prefab de efecto particulas al posicionar un objeto en el plano")]
    public GameObject particleExplosion;

    // GameObject del mantel o empapelado en la jerarquia - solo activa / desactiva
    public GameObject mantelObject;


    // Bloque de informacion adicional - Ahora lo maneja el windows handler
    //public RectTransform bloqueInfo;


    private WindowsHandler myWindowsHandler;


    // Plane Type is a virtual construct
    [SerializeField]
    private Transform tabletAreaPlane;
    public Transform TabletAreaPlane
    {
        get
        {
            return tabletAreaPlane;
        }
        set
        {
            tabletAreaPlane = value;
        }
    }

    [Header("Posicion Vector3 de la mesa en tiempo real")]
    public Vector3 tabletAreaPlanePosition;

    [Header("Rotacion Quartenion de la mesa en tiempo real")]
    public Quaternion tabletAreaPlaneRotation;

    //public Transform objectToPlace;
    //public float hitForce = 100f;

    //[Header("Collider correspondiente a la mesa")]
    //public Collider tabletCollider;


    [Header("Listas de Objetos para previsualizar en la vista previa")]
    public List<GameObject> objectsToPlace = new List<GameObject>();


    [Header("Listas de Objetos ya posicionados listos para usar con gestos")]
    public List<GameObject> objectsToGesture = new List<GameObject>();

    //[HideInInspector]
    public Vector3[] positionOffset;

    // variables para determinar cual es objeto seleccionado en todo momento
    [Header("Lugar en el index de un objeto para invocar")]
    public int currentObjectIndex = 0;
    [Header("Objeto de ese index listo para preview")]
    public GameObject currentObject;
    [Header("Objeto de ese index ya posicionado listo para manipular")]
    public GameObject currentObjectPlaced;


    // Variable que controlara la instancia del objeto en vista previa
    private GameObject previewInstance;
    private GameObject manipulationInstance;

    // Determina si existe un arrastre en movimiento desde el panel hacia la zona
    private bool isPreviewBeingDragged = false;

    //
    public bool instanceCreated = false;

    public List<Button> buttons = new List<Button>();


    /// <summary>
    /// Chequea si esta dentro del area en base al resultado del raycast
    /// usar para demas operaciones para saber si el objeto esta dentro del area o no
    /// </summary>
    public bool insideArea = false;

    // Posicion con el touchscript
    public Vector3 touchHandlerPosition;
    [Header("Usar TouchScript para eventos tactiles")]
    public bool useTouchScript = true;


    [Header("Tracker del Listado de Objetos que en tiempo real se iran instanciando")]
    public List<GameObject> objectsTracking = new List<GameObject>();


    /// <summary>
    /// SINGLETON
    /// </summary>
    /// 
    private static objectPlacementController instance;
    public static objectPlacementController Instance
    {
        get
        {
            return instance;
        }
    }
    //private static objectPlacementController instance;

    private void OnEnable()
    {
        if (TouchManager.Instance != null)
        {
            TouchManager.Instance.TouchesBegan += touchesBeganHandler;
            TouchManager.Instance.TouchesMoved += touchesMovedHandler;
            TouchManager.Instance.TouchesEnded += touchesEndedHandler;
        }
    }

    private void OnDisable()
    {
        if (TouchManager.Instance != null)
        {
            TouchManager.Instance.TouchesBegan -= touchesBeganHandler;
            TouchManager.Instance.TouchesMoved -= touchesMovedHandler;
            TouchManager.Instance.TouchesEnded -= touchesEndedHandler;

        }
    }


    private void touchesMovedHandler(object sender, TouchEventArgs e)
    {

        //Debug.Log(e.Touches[0]);
        //Debug.Log(e.Touches[0].Tags + " touched hold at " + e.Touches[0].Position);

        // Manejo del touch pointer raw o crudo para tomar posicion del objeto mientras se arrastra
        // Se asegura que exista un unico toque
        if (e.Touches.Count == 1)
        {
            foreach (var item in e.Touches)
            {
                touchHandlerPosition = item.Position;
            }
        }
        /*
        else
        {
            isPreviewBeingDragged = false;
            Destroy(previewInstance);
        }
        */

        //touchHandlerPosition = e.Touches[0].Position;

    }

    private void touchesBeganHandler(object sender, TouchEventArgs e)
    {

        if (e.Touches.Count == 1)
        {
            foreach (var item in e.Touches)
            {
                touchHandlerPosition = item.Position;
            }
        }


        foreach (var item in e.Touches)
        {
            renewCounter = 0;

            renewScreenSaverCounter = 0;

            StartCoroutine(ScreenSaverDeActivate());
        }


    }

    private void touchesEndedHandler(object sender, TouchEventArgs e)
    {

        foreach (var item in e.Touches)
        {

            activateCounter = true;

            activateScreenSaverCounter = true;

            //Debug.Log("COUNTER ACTIVATION");

        }


    }


    void Awake()
    {
        if(instance == null)
        {

            instance = this;

        } else if(instance != null){

            Destroy(this);

        }

        DontDestroyOnLoad(this);
    }
    

    // Use this for initialization
    void Start () {

        // Desde el inicio de la app empieza la deteccion del screensaver
        activateScreenSaverCounter = true;


        // Referencia en cache para componente WindowsHandler
        myWindowsHandler = GetComponent<WindowsHandler>();

        if (cursorIsVisible)
        {
            Cursor.visible = cursorIsVisible;
        } else
        {
            // Si la opcion esta como falsa oculta el cursor - el visualizer y deshabilita el debug
            Cursor.visible = cursorIsVisible;
            //ST// visualizerObject.SetActive(false);
            Debug.unityLogger.logEnabled = false;
        }


        //tabletCollider = tabletCollider.GetComponent<BoxCollider>();

        //Inicializacion de la variable array de vector 3
        positionOffset = new Vector3[objectsToPlace.Count];

        for (int i = 0; i < objectsToPlace.Count; i++)
        {
            // De la mesa no toma meshfilter, del empapelado tampoco
            if (currentObjectIndex != 0)
            {
                Mesh myMesh = objectsToPlace[i].GetComponent<MeshFilter>().sharedMesh;

                Bounds myBounds = myMesh.bounds;

                positionOffset[i] = new Vector3(myBounds.size.x / 2 * objectsToPlace[i].transform.localScale.x, myBounds.size.y / 2 * objectsToPlace[i].transform.localScale.y, myBounds.size.z / 2 * objectsToPlace[i].transform.localScale.z);
                //positionOffset = new[] { objectsToPlace.Count };

            }
        }

        //positionOffset = new Vector3(myBounds.size.x / 2 * objectsToPlace[0].transform.localScale.x, myBounds.size.y / 2 * objectsToPlace[0].transform.localScale.y, myBounds.size.z / 2 * objectsToPlace[0].transform.localScale.z);

        /*
        for (int i = 0; i <= buttons.Count; i++)
        {
            Button btn = buttons[i].GetComponent<Button>();
            btn.onClick
            
        }
        */
    }

    // Update is called once per frame
    void Update () {

        //TODO: CREAR SINGLETON Y CHEQUEAR SI ESTA EN MODO EDICION
        //
        //
        //
        tabletAreaPlanePosition = tabletAreaPlane.transform.position;


        //Contador de tiempo para resetear la mesa
        if (activateCounter)
        {

            renewCounter += Time.deltaTime;

            //Debug.Log(renewCounter);

        }


        if(renewCounter > renewSeconds)
        {


            renewCounter = 0;

            RenewTable();

            Debug.Log("Renewing table.........");


        }


        //Contador de tiempo para mostrar el screensaver de la mesa
        if (activateScreenSaverCounter)
        {

            renewScreenSaverCounter += Time.deltaTime;

            //Debug.Log(renewCounter);

        }


        if (renewScreenSaverCounter > renewScreenSaverSeconds)
        {


            renewScreenSaverCounter = 0;

            StartCoroutine(ScreenSaverActivate());

            Debug.Log("Activating screensaver.........");


        }


        //tabletAreaPlaneRotation = tabletAreaPlane.transform.rotation;


        /*
        if (Input.GetMouseButtonDown(0))
        {

            ObjectPlacementMode();

        }

        
        if (Input.GetMouseButton(0))
        {

            print("is being dragged!!!");
            ButtonObjectInstantiatePreview();

        }

        if (Input.GetMouseButtonUp(0))
        {

            ObjectSnap();

        }
        */


    }




    // Asigna en pointer event - Metodos que seran llamados desde los eventtriggers de los botones
    public void ButtonDrag(int newObject)
    {

        //print("is being dragged!!!");

        isPreviewBeingDragged = true;
        

        currentObject = objectsToPlace[newObject];

        currentObjectIndex = newObject;

        //ObjectPlacementMode(currentObjectIndex);

        // Separar objetos para manipular por separado
        currentObjectPlaced = objectsToGesture[newObject];

        
        ////////////  SACAR TEMPORALMENTE //////////////////////////

        
        if (!instanceCreated)
            {

                instanceCreated = !instanceCreated;

            // Saco el quartenion.identity e instancio creando objeto aplicando la rotacion que tiene el plano de la mesa
            if (!useTouchScript)
            {
                previewInstance = (GameObject)Instantiate(currentObject, Input.mousePosition, tabletAreaPlane.transform.rotation);//GameObject.Instantiate(objectsToPlace[newObject], Input.mousePosition, Quaternion.identity);
            }
            else
            {
                previewInstance = (GameObject)Instantiate(currentObject, touchHandlerPosition, tabletAreaPlane.transform.rotation);//GameObject.Instantiate(objectsToPlace[newObject], Input.mousePosition, Quaternion.identity);

                

            }                                                                                                //Destroy(previewInstance);


            print("INSTANCIA CREADA");

            }

        

        else
        {
            

            InstantiatePreview();

        }
        
        


    }

    // Manejo del objeto duplicado que se instancia al soltar el puntero
    // Asigna en pointer events
    public void ButtonRelease()
    {


        //ObjectSnap(currentObjectIndex);

        //Destroy(previewInstance);

        instanceCreated = false;

        if (insideArea == false)
        {


            //si se libera el boton mientras no estemos posicionado sobre ningun rigidbody elimina la instancia de objeto creada
            isPreviewBeingDragged = false;
            Destroy(previewInstance);
            print("DESTRUIR INSTANCIA DE OBJETO");
            print("termino el drag de preview 2");


        }
        else
        {

            // Ultima posicion del vector que tomara la siguiente instancia a crear
            if (previewInstance != null)
            {
                Vector3 lastPosition = previewInstance.transform.position;

                Destroy(previewInstance, 0.1f);


                //manipulationInstance = (GameObject)Instantiate(currentObjectPlaced, touchHandlerPosition, Quaternion.identity);



                isPreviewBeingDragged = false;
                print("termino el drag de preview 1");

                //manipulationInstance = (GameObject)Instantiate(currentObjectPlaced, Camera.main.ScreenToWorldPoint(touchHandlerPosition), Quaternion.identity);

                // Objeto correspondiente al mantel
                if (currentObjectIndex == 0)
                {
                    // Habilita el mantel
                    mantelObject.SetActive(true);

                    mantelObject.GetComponent<Renderer>().material = mantelObject.GetComponent<FadeObjectInOut>().StandardMaterial;

                    mantelObject.GetComponent<SkinnedMeshRenderer>().enabled = true;

                    Debug.Log("Habilita el mantel");
                }

                
                else if (currentObjectIndex == 15)
                {
                    // Habilita el empapelado
                    if (GameObject.FindGameObjectWithTag("Empapelado").GetComponent<RawImage>().color == new Color(1.0f, 1.0f, 1.0f, 0.0f))
                    {
                        myWindowsHandler.learpON = true;
                        myWindowsHandler.learpStart = Time.time;
                        Debug.Log("sacar /  empapelado empapelado");

                        Debug.Log("Habilita el empapelado");
                    }
                }
                

                else
                { 

                    RaycastHit hit;
                    Ray ray = Camera.main.ScreenPointToRay(touchHandlerPosition);

                    if (Physics.Raycast(ray, out hit, 100))
                    {
                        //if (hit.collider.tag == "Mesa")
                        //{
                        print("HIT MESA AT POSITION: " + hit.point);

                        //if (hit.rigidbody != null)
                        //{
                        print("HIT RIGIDBODY POSITION: " + hit.point);
                        manipulationInstance = (GameObject)Instantiate(currentObjectPlaced, touchHandlerPosition, tabletAreaPlane.transform.rotation); //Quaternion.identity);
                                                                                                                                                       //ST 2 // manipulationInstance.transform.position = hit.point;
                        manipulationInstance.transform.position = lastPosition;
                        print("instancia duplicado");


                        // Agrega el objeto instanciado a lista de objetos creados
                        objectsTracking.Add(manipulationInstance);

                        GameObject prefabInstance = (GameObject)Instantiate(particleExplosion, hit.point, Quaternion.identity);//, touchHandlerPosition, tabletAreaPlane.transform.rotation); //Quaternion.identity);

                        Destroy(prefabInstance, 2f);

                        //}

                    }

                }

            }


            

        }

        
    }





    // TODO OBJECT PLACEMENT MIENTRAS SOSTIENE EL MOUSE
    void ObjectPlacementMode(int index)
    {

        /*
        if (Input.GetMouseButtonUp(0) && !insideArea)
        {
            for (int i = 0; i <= objectsToPlace.Count; i++)
            {

                Destroy(objectsToPlace[i]);

            }


        }
        */

    }


    public void InstantiatePreview()
    {





        if (!useTouchScript)
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit, 100))
            {

                if (hit.rigidbody != null)
                {
                    // la posicion del objeto que tenga este script es igual a la posicion del hit point
                    previewInstance.transform.position = hit.point; //+ objectPlacementController.instance.positionOffset[i];

                    previewInstance.tag = "Created";

                    insideArea = true;

                }

            }
            else
            {

                //insideArea = false;
                Vector3 tempMouse = Input.mousePosition;// Camera.main.ScreenToViewportPoint (Input.mousePosition);

                tempMouse.z = 10f;

                previewInstance.transform.position = Camera.main.ScreenToWorldPoint(tempMouse);

                print("POSICION PUNTERO: " + previewInstance.transform.position);

                insideArea = false;

            }
        }

        else
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(touchHandlerPosition);

            if (Physics.Raycast(ray, out hit, 100))
            {
                // Detecte que esta dentro de Todo lo que tenga etiqueta Mesa (Madera y bordes) o mas objetos
                if (hit.collider.tag == "Mesa")
                {
                    // la posicion del objeto que tenga este script es igual a la posicion del hit point

                    // ST // ----- CAMBIAR POR INSTANCIA

                    //Ya esta implementada la logica para los offset en base una variable global - pero mejor que los objetos tengan el centro en la misma base
                    previewInstance.transform.position = hit.point; //+ objectPlacementController.instance.positionOffset[i];

                    previewInstance.tag = "Created";

                    //manipulationInstance = (GameObject)Instantiate(currentObjectPlaced, touchHandlerPosition, Quaternion.identity);

                    //manipulationInstance.transform.position = hit.point;

                    //manipulationInstance.tag = "Created";

                    insideArea = true;

                    print("release object");

                }

            }
            else
            {

                //insideArea = false;
                Vector3 tempMouse = touchHandlerPosition;// Camera.main.ScreenToViewportPoint (Input.mousePosition);

                tempMouse.z = 10f;

                previewInstance.transform.position = Camera.main.ScreenToWorldPoint(tempMouse);

                //print("POSICION PUNTERO: " + previewInstance.transform.position);

                insideArea = false;
            }





        }

    }


    public IEnumerator ScreenSaverActivate()
    {
        if (!ScreenSaver.activeInHierarchy)
        {
            ScreenSaver.gameObject.SetActive(true);
            ScreenSaver.GetComponent<Animator>().SetTrigger("idle");

            yield return null;
        }
    }


    public IEnumerator ScreenSaverDeActivate()
    {
        if (ScreenSaver.activeInHierarchy)
        {

            ScreenSaver.GetComponent<Animator>().SetTrigger("exit");

            yield return new WaitForSeconds(1.5f);

            ScreenSaver.gameObject.SetActive(false);

            yield return null;
            
        }
    }


    public void RenewTable()
    {

        if (objectPlacementController.Instance.objectsTracking != null) {

            Debug.Log("mantiene apretado por 3 segundos..................");

            foreach (var item in objectPlacementController.Instance.objectsTracking)
            {
                item.GetComponent<Renderer>().material = item.GetComponent<FadeObjectInOut>().FadeMaterial;

                //Desvanece el GameObject gradualmente hasta desaparecerlo
                item.GetComponent<FadeObjectInOut>().FadeOut();


                Destroy(item.gameObject, 3f);

            }

            objectPlacementController.Instance.objectsTracking.Clear();


        }

        if (mantelObject.activeInHierarchy)
        {

            mantelObject.GetComponent<Renderer>().material = mantelObject.GetComponent<FadeObjectInOut>().FadeMaterial;

            mantelObject.GetComponent<FadeObjectInOut>().FadeOut();



            Invoke("HideMantel", 3f);
        }

        if (myWindowsHandler.empapeladoObject)
        {

            if (GameObject.FindGameObjectWithTag("Empapelado").GetComponent<RawImage>().color == new Color(1.0f, 1.0f, 1.0f, 1.0f))
            {

                myWindowsHandler.learpOFF = true;
                myWindowsHandler.learpStart = Time.time;
                Debug.Log("hide empapelado");

            }
            
        }
    }






    public void HideMantel()
    {
        mantelObject.SetActive(false);
        Debug.Log("Set active false mantel");
    }


    /*
    void ObjectDestroy()
    {

        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hit, 100))
        {

            if (hit.collider == null)
            {


                //si se libera el boton mientras no estemos posicionado sobre ningun rigidbody elimina la instancia de objeto
                Destroy(previewInstance);


            }





        }

    }
    */

    /*
    // TODO SNAP DE LOS OBJETOS CUANDO SUELTA EL MOUSE
    void ObjectSnap(int index)
    {

        //if (insideArea)
        //{

            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, 100))
                if (hit.rigidbody != null)

                    //hit.rigidbody.AddForceAtPosition(ray.direction * pokeForce, hit.point);

                    //hit.collider.gameObject

                    //GameObject.Instantiate(objectsToPlace[0], new Vector3(hit.point.x, tabletAreaPlanePosition.y + positionOffset.y, hit.point.z) , Quaternion.identity);

                    //Object.Destroy(previewInstance, 0f);

                    //Destroy(previewInstance);

                    //instanceCreated = false;

                    //TODO: ELIMINAR LA CREACION DE INSTANCIA EN EL SNAP Y DEJAR SOLAMENTE LA CREACION CON EL BOTON?
                    GameObject.Instantiate(objectsToPlace[index], hit.point, Quaternion.identity);// + positionOffset[index], Quaternion.identity); //+ currentObject.GetComponent<MeshFilter>().sharedMesh.bounds.size / 2, Quaternion.identity);//+ positionOffset[index], Quaternion.identity);


        //}

        //Destroy(previewInstance);
    }
    */

    /*
    public void DisplayInfo()
    {

        bloqueInfo.gameObject.SetActive(true);

    }

    public void HideInfo()
    {

        bloqueInfo.gameObject.SetActive(false);

    }
    */

}
