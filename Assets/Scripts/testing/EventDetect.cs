﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class EventDetect : MonoBehaviour, IPointerEnterHandler, IDragHandler
{

    private void Awake()
    {
        
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    /*
    public override void OnDrag(PointerEventData data)
    {
        Debug.Log("OnDrag called.");
    }
    */
    public void OnDrag(PointerEventData eventData)
    {
        Debug.Log("Calling Drag");
    }
    

    
    //Do this when the cursor enters the rect area of this selectable UI object.
    public void OnPointerEnter(PointerEventData eventData)
    {
        Debug.Log("The cursor entered the selectable 3D element.");
    }
    

}

