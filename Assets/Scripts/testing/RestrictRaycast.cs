﻿using System.Collections;
using System.Collections.Generic;
using TouchScript;
using TouchScript.Behaviors;
using TouchScript.Gestures;
using UnityEngine;

public class RestrictRaycast : MonoBehaviour {
    
    private Vector3 touchMovePosition;
    private bool isMoving = false;

    TransformGesture dragGesture;

    private void OnEnable()
    {

        dragGesture = GetComponent<TransformGesture>();

        dragGesture.Transformed += touchesMovedHandler;

        
    }

    private void OnDisable()
    {
        dragGesture.StateChanged -= touchesMovedHandler;
    }

    private void touchesMovedHandler(object sender, System.EventArgs e)
    {

        Debug.Log("IS MOVING.............." + dragGesture.DeltaPosition);
        //Debug.Log(e.Touches[0]);
        //Debug.Log(e.Touches[0].Tags + " touched hold at " + e.Touches[0].Position);

        //touchMovePosition = e.Touches[0].Position;
        //isMoving = true;

        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(transform.position);
        if (Physics.Raycast(ray, out hit, 100))
        {

            Debug.Log("IS MOVING 2....................");

            if (hit.rigidbody != null)
            //if(hit.collider.isTrigger && hit.collider.gameObject.tag == "Mesa" && isMoving)
            //if (hit.collider.gameObject.tag == "Created")
            {


                    // la posicion del objeto que tenga este script es igual a la posicion del hit point
                    transform.position = hit.point; //+ objectPlacementController.instance.positionOffset[i];
                    Debug.Log("POSICION MOUSE: " + Input.mousePosition);




            }


            else
            {

                return;
                //insideArea = false;


            }

        }

    }


    private void touchesEndedHandler(object sender, TouchEventArgs e)
    {

        //Debug.Log(e.Touches[0]);
        //Debug.Log(e.Touches[0].Tags + " touched hold at " + e.Touches[0].Position);

        isMoving = false;

    }
    
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        //if (insideArea)
        //{
        //Vector3 cursorScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);
        //Vector3 cursorPosition = Camera.main.ScreenToWorldPoint(cursorScreenPoint) + offset;
        //transform.position = cursorPosition;

        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(this.gameObject.transform.position);

        //if(GetComponent<Collider>().Raycast(ray, out hit, 100))

        if (Physics.Raycast(ray, out hit, 100))
        {

            if (hit.rigidbody != null)
            //if(hit.collider.isTrigger && hit.collider.gameObject.tag == "Mesa" && isMoving)
            //if (hit.collider.gameObject.tag == "Created")
            {

                // Funcion que llama interfaz mientras hago drag
                //ST objectPlacementController.Instance.DisplayInfo();
                /*
                for (int i = 0; i < objectPlacementController.Instance.objectsToPlace.Count; i++)
                {

                    // la posicion del objeto que tenga este script es igual a la posicion del hit point
                    transform.position = hit.point; //+ objectPlacementController.instance.positionOffset[i];
                    Debug.Log("POSICION MOUSE: " + Input.mousePosition);

                }
                */
                //this.gameObject.transform.position = hit.point; //+ objectPlacementController.instance.positionOffset[i];
                //Debug.Log("POSICION MOUSE: " + Input.mousePosition);

                transform.position = hit.point;


                return;

            }


            else
            {

                //insideArea = false;

                return;


            }

        }

    }

    
}
