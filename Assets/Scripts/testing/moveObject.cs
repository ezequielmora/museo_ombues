﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveObject : MonoBehaviour {

    public float distance = 1f;

    private Transform myTransform;

	// Use this for initialization
	void Start () {

        myTransform = this.gameObject.transform;
		
	}
	
	// Update is called once per frame
	void Update () {

        

    }

    private void OnMouseDrag()
    {

        Vector3 mousePosition = Input.mousePosition;

        mousePosition.z = distance;

        myTransform.position = Camera.main.ScreenToWorldPoint(mousePosition);

    }
}
