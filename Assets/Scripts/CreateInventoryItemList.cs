﻿#if UNITY_EDITOR
using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;
/*
public class CreateInventoryItemList
{
[MenuItem("Assets/Create/Museo Ombues/Inventory Item List")]
public static dataList Create()
{
dataList asset = ScriptableObject.CreateInstance<dataList>();

AssetDatabase.CreateAsset(asset, "Assets/InventoryItemList.asset");
AssetDatabase.SaveAssets();
return asset;
}
}
*/

[CreateAssetMenu(fileName = "DB/itemsData", menuName = "Museo/Utensillos", order = 1)]
public class MemoPezConfig : ScriptableObject
{
    [Header("Objetos")]
    public List<dataItem> items = new List<dataItem>();

    [Header("Factor de puntuacion")]
    public int scoreFactor;

    [Header("Bonus")]
    public int bonusPoints;
}

#endif