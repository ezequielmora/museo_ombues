﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class webcamController : MonoBehaviour {

    //public Transform myCameraPlane;

    [Header("Habilita la webcam si la misma esta conectada correctamente")]
    public bool cameraEnable = true;

    public GameObject myRawImageObject;

    private RawImage myRawImage;

    private WebCamDevice myWebCam;

    private WebCamTexture myWebCamTexture;






    public static webcamController instance;


    //Awake is always called before any Start functions
    void Awake()
    {
        //Chequea si la instancia del singleton ya existe
        if (instance == null)

            //si no existe, setea la instacia a esta clase
            instance = this;

        //si la instancia ya existe pero no esta, la destruye
        else if (instance != this)

            //destruya esa instancia reforzando el singleton, y asegurando que sea la unica clase existente en todo momento
            Destroy(gameObject);

        //Setea que esta instancia no se destruya por cambios de escenas
        DontDestroyOnLoad(gameObject);



        //Funcion de inicializacion
        //InitGame();
    }


    // Use this for initialization
    void Start () {

        myRawImage = myRawImageObject.GetComponent<RawImage>();

        Init();


	}
	
	// Update is called once per frame
	void Init () {

        if (cameraEnable)
        {
            myWebCamTexture = new WebCamTexture(1920, 1080);


            //myCameraRender.material.mainTexture = myWebCamTexture;

            myRawImage.material.mainTexture = myWebCamTexture;

            // Pongo la webcam a maxima calidad
            myWebCamTexture.filterMode = FilterMode.Trilinear;

            // Check for devices cameras
            if (WebCamTexture.devices.Length == 0)
            {

                Debug.Log("NO CAMERA DEVICES FOUND");
                return;


            }
            else

            {

                Debug.Log("CAMERA DEVICES FOUND");

                //Renderer myCameraRender = myCameraPlane.GetComponent<Renderer>();




                StartCamera();

            }

        }

    }


    public void StartCamera()
    {


        if (WebCamTexture.devices.Length == 1)
        {

            myWebCamTexture.Play();

        }

        


    }



    public void StopCamera()
    {

        if(myWebCamTexture != null)
        myWebCamTexture.Stop();


    }

}
